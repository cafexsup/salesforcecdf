#Sales Force Call Center Demo

This repository represents the various components required to setup a SalesForce CTI call center demo.

To run this demo you will need:

1. A SalesForce developer account (please note this demo has not been tested in a production environment, so either user your developer sandbox if you have a production SalesForce account, or sign up for a free developer account here -   [https://developer.salesforce.com/signup](https://developer.salesforce.com/signup))

2. A CafeX installation environment including FCSDK

3. A webserver, either within your network or running locally (this demo assumes you're using a PHP server e.g. MAMP Pro)

4. A code editor e.g. sublimetext

5. A PSTN gateway, to route your SIP Outbound traffic from FCSDK. e.g. Twilio

## Setup Instructions

### SalesForce - Install the Demo Call Center Adapter provided by SalesForce:

1. Login to your SF developer account at login.salesforce.com
2. Install the Demo Call Center Adapter provided by SalesForce, by navigating here  - https://login.salesforce.com/packaging/installPackage.apexp?p0=04t80000000tn5y
3. Click the "Setup" link (top right)
4. In the lhs sidebar expand Build -> Customize -> Call Center and select Call Centers
5. You should see the Demo Call Center Adpater installed, select it and click "Manage Users" button and remove yourself / users

### SalesForce - Import your own Call Center Adapter

1. Click the "Setup" link (top right)
2. In the lhs sidebar expand Build -> Customize -> Call Center and select Call Centers
3. Click the "Import" and import the callCenterDefnition.xml packaged in this repository.
4. Select your new Call Center and click "Manage Users" button and add yourself / your users.

### SalesForce - Create your VisualForce SoftPhone InterFace

1. Click the "Setup" link (top right)
2. In the lhs sidebar expand Build -> Develop -> VisualForce Pages and click the "New" button
3. Open the callcenter.html file in this repo. Be sure to change references to https://richard-mbp.cafex.com to point to your session provisioning PHP (MAMP) server.
4. In the embedded code editor, in SF, paste in the apex:page code from the callcenter.html file in this repo, name it callcenter and click save. 

### MAMP PRO / FCSDK Setup

1. You need to deploy the session.php and call.html files in this repo to your MAMP PRO, prferably in a /salesforce folder.
2. You need to setup your MAMP Pro server for https / ssl trafic, using self signed certs feature of MAMP PRO.
3. Test you can get a sessiion token by navigating to the session.php script in your browser.

### FCSDK - CM / PSTN SIP Outbound Gateway

1. You need to set an outbound sip address in the FCSDK Web Plugin Framework under Gateway -> SIP Global Configuration -> Outbound SIP Servers
2. To route calls correctly, you'll need to edit the call number handling code in call.html to call valid SIP url strings for your outbound domain.

## Useful Links

SF Open CTI Adapter
[https://developer.salesforce.com/page/Open_CTI](https://developer.salesforce.com/page/Open_CTI)

SF Open CTI Developer Guide
[https://developer.salesforce.com/docs/atlas.en-us.api_cti.meta/api_cti/](https://developer.salesforce.com/docs/atlas.en-us.api_cti.meta/api_cti/)

